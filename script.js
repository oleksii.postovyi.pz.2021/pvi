
const btnAddStudent = document.querySelector(".btn--add-student");
const studentWindow = document.querySelector(".student-window");
const student = {group: "", name: "", gender: "", birthday: ""}
let editable;

if ("serviceWorker" in navigator) {
    self.addEventListener("load", async () => {
        const container = navigator.serviceWorker;
        if (container.controller === null) {
            const reg = await container.register("sw.js");
        }
    });
}

function addStudent() {
    fetch('http://localhost/lab3/index.php', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
            },
        body: JSON.stringify({
            group: document.forms["studentForm"]["group"].value,
            "full-name": document.forms["studentForm"]["full-name"].value,
            birthday: document.forms["studentForm"]["birthday"].value,
            gender: document.forms["studentForm"]["gender"].value
        }),
    }).then((response) => {
        if (response.status == 200)
        {
            let table = document.getElementById("tbl");
            let row = table.insertRow(table.length);
            let cell1 = row.insertCell(0);
            let cell2 = row.insertCell(1);
            let cell3 = row.insertCell(2);
            let cell4 = row.insertCell(3);
            let cell5 = row.insertCell(4);
            let cell6 = row.insertCell(5);
            let cell7 = row.insertCell(6);
            cell1.innerHTML = '<input type="checkbox">'
            cell2.innerHTML = student.group = document.forms["studentForm"]["group"].value;
            cell3.innerHTML = student.name = document.forms["studentForm"]["full-name"].value;
            cell5.innerHTML = student.birthday = document.forms["studentForm"]["birthday"].value;
            cell4.innerHTML = student.gender = document.forms["studentForm"]["gender"].value;
            console.log(JSON.stringify(student))
            cell6.innerHTML = '<img style="max-width: 30px; max-height: 30px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Ski_trail_rating_symbol-green_circle.svg/1200px-Ski_trail_rating_symbol-green_circle.svg.png">'
            cell7.innerHTML = '<button onclick="openEditStudentWindow(this.parentNode.parentNode)" style = "margin-right: 1%; width: 25px; height: 25px;">' +
            '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">\n' +
            '  <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>\n' +
            '</svg></button><button style = "width: 25px; height: 22px;" onclick="removeStudent(this.parentNode.parentNode)">X</button>'
        }
        else (response.json().then(json => {alert(json.error)}))
    }).catch((err) => {
        alert(err);
    });
}

function editStudent() {
    fetch('http://localhost/lab3/index.php', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
            },
        body: JSON.stringify({
            group: document.forms["studentForm"]["group"].value,
            "full-name": document.forms["studentForm"]["full-name"].value,
            birthday: document.forms["studentForm"]["birthday"].value,
            gender: document.forms["studentForm"]["gender"].value
        }),
    }).then((response) => {
        if (response.status == 200)
        {
            editable.innerHTML = '<tr><td><input type="checkbox"></td><td>'
            +document.forms["studentForm"]["group"].value+'</td><td>'
            +document.forms["studentForm"]["full-name"].value+'</td><td>'
            +document.forms["studentForm"]["birthday"].value+'</td><td>'
            +document.forms["studentForm"]["gender"].value+'</td><td><img style="max-width: 30px; max-height: 30px;" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Ski_trail_rating_symbol-green_circle.svg/1200px-Ski_trail_rating_symbol-green_circle.svg.png"></td><td><button onclick="openEditStudentWindow(this.parentNode.parentNode)" style="margin-right: 1%; width: 25px; height: 25px;"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16"><path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"></path></svg></button><button style="width: 25px; height: 22px;" onclick="removeStudent(this.parentNode.parentNode)">X</button></td></tr>'
        }
        else (response.json().then(json => {alert(json.error)}))
    }).catch((err) => {
        alert(err);
    });
}

function removeStudent(el) {
    if(confirm("Are you sure you want to delete user?"))
        el.remove();
}

function openEditStudentWindow(el) {
    editable = el;
    console.log(editable)
    student.group = editable.children[1].textContent
    student.name = editable.children[2].textContent
    student.gender = editable.children[3].textContent
    student.birthday = editable.children[4].textContent
    studentWindow.classList.add("opened");
    document.getElementById("group").value = student.group
    document.getElementById("full-name").value = student.name
    document.getElementById("gender").value = student.gender
    document.getElementById("birthday").value = student.birthday
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Edit student";
    let butt = document.getElementById("b1");
    butt.onclick = editStudent;
}

function openAddStudentWindow() {
    studentWindow.classList.add("opened");
    const studentWindowHeading = studentWindow.querySelector("h3");
    studentWindowHeading.innerHTML = "Add student";
    document.querySelectorAll("input").forEach((input) => (input.value = ""));
    document.querySelectorAll("select").forEach((select) => (select.value = ""));
    let butt = document.getElementById("b1");
    butt.onclick = addStudent;
}
  
function closeStudentWindow() {
    studentWindow.classList.remove("opened");
}

  