<?php
error_reporting(E_ERROR | E_PARSE);
$gropList = array('PZ-25', 'PZ-24', 'PZ-23', 'PZ-22', 'PZ-21', 'PZ-26');
$genderList = array('Male', 'Female', 'Undefined');
function verify($name,$group,$gender,$birthday){
	$nameRegex = "/^[a-zA-Zа-яА-Я]{2,}$/";
	global $gropList, $genderList;
	$response = [
		"error" =>''
	];
  if (!preg_match($nameRegex ,$name)) {
    $response["error"] = "Name is incorrect!";
  }
  
	if(!in_array($group,$gropList)){
		$response["error"] .= "\nGroup is incorrect!";
	}
	
	if(!in_array($gender,$genderList)){
		$response["error"] .= "\nGender is incorrect!";
	}

  $minYear = 1900;
  $maxYear = 2006;
  $currentYear = date('Y', strtotime($birthday));

  if ($currentYear > $maxYear || $currentYear < $minYear) {
		$response["error"] .= "\nDate birthday must be between " . $minYear . " and " . $maxYear;
  }
	return $response;
}



if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $data = json_decode(file_get_contents('php://input'), true);
  $fullName = $data['full-name'];
  $group = $data['group'];
  $gender = $data['gender'];
  $birthday = $data['birthday'];
	$response = verify($fullName,$group,$gender,$birthday);

	if($response["error"] == ''){
		$response = [
			"id"=>uniqid(),
			"full-name" => $fullName,
			"birthday" => $birthday,
			"gender" => $gender,
			"group" => $group
		];
		echo json_encode($response);
		exit();
	}
	http_response_code(400);
  echo json_encode($response);
}
?>